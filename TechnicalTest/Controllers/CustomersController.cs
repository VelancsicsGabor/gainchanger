﻿using System;
using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace TechnicalTest.Controllers
{
    /// <summary>
    /// Controller for customers
    /// </summary>
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        #region Fields

        /// <summary>
        /// The customers manager
        /// </summary>
        private readonly ICustomersManager _customersManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController" /> class.
        /// </summary>
        /// <param name="customersManager">The customers manager.</param>
        public CustomersController(ICustomersManager customersManager)
        {
            _customersManager = customersManager ?? throw new ArgumentNullException(nameof(customersManager));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes an existing Customer.
        /// </summary>
        /// <remarks>
        ///  
        ///     DELETE api/customers/5
        ///      
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Successful delete response.</returns>
        /// <response code="204">Returns when Customer deleted.</response>
        /// <response code="404">Returns when Customer not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Delete(int id)
        {
            if (!_customersManager.DoesCustomerExists(id))
            {
                return NotFound(id);
            }

            _customersManager.DeleteCustomer(id);
            return NoContent();
        }

        /// <summary>
        /// Gets all Customers
        /// </summary>
        /// <remarks>
        ///  
        ///      GET api/customers
        ///      
        /// </remarks>
        /// <returns>Successful list of response.</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<CustomerDto>> Get()
        {
            return Ok(_customersManager.GetCustomers());
        }

        /// <summary>
        /// Gets an existing Customer.
        /// </summary>
        /// <remarks>
        ///  
        ///      GET api/customers/5
        /// 
        /// </remarks> 
        /// <param name="id"></param>
        /// <returns>Successful get response.</returns>
        /// <response code="200">Returns when requested resource provided.</response>
        /// <response code="404">Returns when Customer not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<CustomerDto> Get(int id)
        {
            var customer = _customersManager.GetCustomer(id);
            if (customer == null)
            {
                return NotFound(id);
            }

            return Ok(customer);
        }

        /// <summary>
        /// Creates a new Customer.
        /// </summary>
        /// <remarks>
        ///  
        ///      POST api/customers
        /// 
        /// </remarks> 
        /// <param name="customerDto"></param>
        /// <returns>Successful creation response.</returns>
        /// <response code="201">Returns when the newly created item persisted.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<CustomerDto> Post([FromBody] CustomerCreateRequestDto customerDto)
        {
            return Created(nameof(Get), _customersManager.Add(customerDto));
        }

        /// <summary>
        /// Updates a Customer.
        /// </summary>
        /// <remarks>
        ///  
        ///      PUT api/customers/5
        /// 
        /// </remarks> 
        /// <param name="customerDto"></param>
        /// <returns>Successful updated response.</returns>
        /// <response code="200">Returns when the updated created item persisted.</response>
        /// <response code="404">Returns when Customer not found.</response>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<CustomerDto> Put([FromBody] CustomerDto customerDto)
        {
            if (!_customersManager.DoesCustomerExists(customerDto.Id))
            {
                return NotFound(customerDto.Id);
            }

            return Ok(_customersManager.Update(customerDto));
        }

        #endregion
    }
}