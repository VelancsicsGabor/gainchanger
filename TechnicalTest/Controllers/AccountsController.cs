﻿using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace TechnicalTest.Controllers
{
    /// <summary>
    /// Controller for customers
    /// </summary>
    [Route("api/accounts")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        #region Fields

        /// <summary>
        /// The customers manager
        /// </summary>
        private readonly ICustomersManager _customersManager;

        /// <summary>
        /// The account manager
        /// </summary>
        private readonly IAccountManager _accountManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController" /> class.
        /// </summary>
        /// <param name="customersManager">The customers manager.</param>
        /// <param name="accountManager">The account manager</param>
        public AccountsController(ICustomersManager customersManager, IAccountManager accountManager)
        {
            _customersManager = customersManager ?? throw new ArgumentNullException(nameof(customersManager));
            _accountManager = accountManager ?? throw new ArgumentNullException(nameof(accountManager));
        }

        #endregion

        #region Methods

        /// <summary>
        /// GetAvailableFunds
        /// </summary>
        /// <remarks>
        ///  
        ///      GET api/accounts/1
        /// 
        /// </remarks> 
        /// <param name="id">Customer id</param>
        /// <returns>The available funds</returns>
        /// <response code="200">Returns when the funds are available.</response>
        /// <response code="404">Returns when the customer not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<FundResponseDto> Get([Required] int id)
        {
            if (!_customersManager.DoesCustomerExists(id))
            {
                return NotFound(id);
            }

            return Ok(new FundResponseDto { Id = id, Funds = _accountManager.GetAvailableFunds(id) });
        }

        /// <summary>
        /// Deposit
        /// </summary>
        /// <remarks>
        ///  
        ///      POST api/accounts/1/deposit
        /// 
        /// </remarks> 
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>The available funds</returns>
        /// <response code="200">Returns when the deposit persisted.</response>
        /// <response code="400">Returns when the deposit can not be completed.</response>
        /// <response code="404">Returns when the customer not found.</response>
        [HttpPost("{id}/deposit")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<FundResponseDto> Deposit([Required] int id, [Required][FromBody] FundRequestDto funds)
        {
            if (!_customersManager.DoesCustomerExists(id))
            {
                return NotFound(id);
            }

            var result = _accountManager.DepositFunds(id, funds.Funds);
            if (result.IsSuccess)
            {
                return Ok(new FundResponseDto { Id = id, Funds = _accountManager.GetAvailableFunds(id) });
            }

            return BadRequest(result.Message);
        }

        /// <summary>
        /// Withdraw
        /// </summary>
        /// <remarks>
        ///  
        ///      POST api/accounts/1/withdraw
        /// 
        /// </remarks> 
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>The available funds</returns>
        /// <response code="200">Returns when the withdraw persisted.</response>
        /// <response code="400">Returns when the withdraw can not be completed.</response>
        /// <response code="404">Returns when the customer not found.</response>
        [HttpPost("{id}/withdraw")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<FundResponseDto> Withdraw([Required] int id, [Required][FromBody] FundRequestDto funds)
        {
            if (!_customersManager.DoesCustomerExists(id))
            {
                return NotFound(id);
            }

            var result = _accountManager.WithdrawFunds(id, funds.Funds);
            if (result.IsSuccess)
            {
                return Ok(new FundResponseDto { Id = id, Funds = _accountManager.GetAvailableFunds(id) });
            }

            return BadRequest(result.Message);
        }

        /// <summary>
        /// Transfer
        /// </summary>
        /// <remarks>
        ///  
        ///      POST api/accounts/transfer
        /// 
        /// </remarks> 
        /// <param name="transferRequest">The transfer request</param>
        /// <returns>The available funds</returns>
        /// <response code="200">Returns when the transfer persisted.</response>
        /// <response code="400">Returns when the transfer can not be completed.</response>
        /// <response code="404">Returns when the customer not found.</response>
        [HttpPost("transfer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<TransferResponseDto> Transfer([Required][FromBody] TransferRequestDto transferRequest)
        {
            if (!_customersManager.DoesCustomerExists(transferRequest.From))
            {
                return NotFound(transferRequest.From);
            }

            if (!_customersManager.DoesCustomerExists(transferRequest.To))
            {
                return NotFound(transferRequest.To);
            }

            var result = _accountManager.TransferFunds(transferRequest.From, transferRequest.To, transferRequest.Funds);
            if (result.IsSuccess)
            {
                return Ok(new TransferResponseDto
                {
                    From = transferRequest.From,
                    FromFunds = _accountManager.GetAvailableFunds(transferRequest.From),
                    To = transferRequest.To,
                    ToFunds = _accountManager.GetAvailableFunds(transferRequest.To)
                });
            }

            return BadRequest(result.Message);
        }

        #endregion
    }
}