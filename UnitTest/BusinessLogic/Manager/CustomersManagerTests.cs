using AutoMapper;
using BusinessLogic;
using BusinessLogic.Manager;
using BusinessLogic.MappingProfiles;
using DataTransferObjects;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Repository.EntityModels;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.BusinessLogic.Manager
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CustomersManagerTests
    {
        private IMapper _mapper;

        [TestInitialize]
        public void TestInit()
        {
            var mapperConfiguration = new MapperConfiguration(c =>
            {
                c.AddProfiles(
                    new List<Profile>
                    {
                        new DtoEntityProfile()
                    });
            });
            _mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingConstructorWithNullRepository_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new CustomersManager(null, null, null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'repository')");
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingConstructorWithNullMapper_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new CustomersManager(Substitute.For<IRepository>(), null, null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'mapper')");
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingConstructorWithNullLocker_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new CustomersManager(Substitute.For<IRepository>(), Substitute.For<IMapper>(), null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'locker')");
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingDeleteExistingCustomer_ThenDeleteCustomerShouldBeCalled()
        {
            // Arrange
            var customer = new CustomerEntity();
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns(customer);
            var manager = new CustomersManager(repo, Substitute.For<IMapper>(), new TransferLocker());

            // Act
            manager.DeleteCustomer(1);

            // Assert
            repo.Received().DeleteCustomer(customer);
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingDeleteNonExistingCustomer_ThenDeleteCustomerShouldNotBeCalled()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns((CustomerEntity)null);
            var manager = new CustomersManager(repo, Substitute.For<IMapper>(), new TransferLocker());

            // Act
            manager.DeleteCustomer(1);

            // Assert
            repo.DidNotReceive().DeleteCustomer(Arg.Any<CustomerEntity>());
        }
        
        [TestMethod]
        public void GivenCustomersManager_WhenCallingGetCustomerWithExistingCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var customer = new CustomerDto {Id = 1, IdCard = "idCard", Name = "name", Surname = "surname"};
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(customer.Id).Returns(_mapper.Map<CustomerEntity>(customer));
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.GetCustomer(customer.Id);

            // Assert
            result.Should().BeEquivalentTo(customer);
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingGetCustomerWithNonExistingCustomer_ThenNullShouldReturn()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns((CustomerEntity)null);
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.GetCustomer(1);

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingGetAllCustomers_ThenProperObjectShouldReturn()
        {
            // Arrange
            var list = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IdCard = "idCard1", Name = "name1", Surname = "surname1"},
                new CustomerEntity {Id = 2, IdCard = "idCard2", Name = "name2", Surname = "surname2"},
                new CustomerEntity {Id = 3, IdCard = "idCard3", Name = "name3", Surname = "surname3"}
            };
            var repo = Substitute.For<IRepository>();
            repo.GetCustomers().Returns(list);
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.GetCustomers();

            // Assert
            result.Should().BeEquivalentTo(_mapper.Map<IEnumerable<CustomerDto>>(list));
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingDoesCustomerExists_ThenProperBoolShouldReturn()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            repo.DoesCustomerExist(1).Returns(true);
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.DoesCustomerExists(1);

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingAddWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            Action act = () => manager.Add(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerDto')");
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingAddWithCustomer_ThenProperObjectShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerCreateRequestDto { IdCard = "idCard", Name = "name", Surname = "surname" };
            var customerEntity = _mapper.Map<CustomerEntity>(customer);
            var repo = Substitute.For<IRepository>();
            repo.AddCustomer(Arg.Any<CustomerEntity>()).Returns(customerEntity);
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.Add(customer);

            // Assert
            result.Should().BeEquivalentTo(customer);
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingUpdateWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            Action act = () => manager.Update(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerDto')");
        }

        [TestMethod]
        public void GivenCustomersManager_WhenCallingUpdateWithCustomer_ThenProperObjectShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var repo = Substitute.For<IRepository>();
            var manager = new CustomersManager(repo, _mapper, new TransferLocker());

            // Act
            var result = manager.Update(customer);

            // Assert
            repo.Received().UpdateCustomer(Arg.Any<CustomerEntity>());
            result.Should().BeEquivalentTo(customer);
        }
    }
}