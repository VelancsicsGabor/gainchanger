using BusinessLogic;
using BusinessLogic.Manager;
using DataTransferObjects;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Repository.EntityModels;
using Repository.Interface;
using System;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.BusinessLogic.Manager
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AccountManagerTests
    {

        [TestMethod]
        public void GivenAccountManager_WhenCallingConstructorWithNullRepository_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new AccountManager(null, null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'repository')");
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingConstructorWithNullLocker_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new AccountManager(Substitute.For<IRepository>(), null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'locker')");
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingGetAvailableFunds_ThenProperNumberShouldBeReceived()
        {
            // Arrange
            var repo = Substitute.For<IRepository>();
            repo.GetAvailableFunds(1).Returns(10);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var funds = manager.GetAvailableFunds(1);

            // Assert
            funds.Should().Be(10);
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingDepositFundsWithExistingCustomer_ThenProperResultShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity {Id = 1, IdCard = "idCard", IsActive = true, Name = "name", Surname = "surname"};
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns(customer);
            repo.DepositFunds(customer, 10).Returns(Result.Success);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.DepositFunds(1, 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingDepositFundsWithNonExistingCustomer_ThenProperResultShouldBeReceived()
        {
            // Arrange
            
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns((CustomerEntity)null);
            
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.DepositFunds(1, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Customer not found, id:1");
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingWithdrawFundsWithExistingCustomer_ThenProperResultShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, IdCard = "idCard", IsActive = true, Name = "name", Surname = "surname" };
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns(customer);
            repo.WithdrawFunds(customer, 10).Returns(Result.Success);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.WithdrawFunds(1, 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingWithdrawFundsWithNonExistingCustomer_ThenProperResultShouldBeReceived()
        {
            // Arrange

            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(1).Returns((CustomerEntity)null);

            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.WithdrawFunds(1, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Customer not found, id:1");
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingTransferFundsWithExistingSenderAndReceiver_ThenProperResultShouldBeReceived()
        {
            // Arrange
            var fromCustomer = new CustomerEntity { Id = 1, IdCard = "idCard1", IsActive = true, Name = "name1", Surname = "surname1" };
            var toCustomer = new CustomerEntity { Id = 2, IdCard = "idCard2", IsActive = true, Name = "name2", Surname = "surname2" };
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(fromCustomer.Id).Returns(fromCustomer);
            repo.GetCustomer(toCustomer.Id).Returns(toCustomer);
            repo.TransferFunds(fromCustomer, toCustomer, 10).Returns(Result.Success);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.TransferFunds(1, 2, 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingTransferFundsWithNonExistingSender_ThenProperResultShouldBeReceived()
        {
            // Arrange
            var fromCustomer = new CustomerEntity { Id = 1, IdCard = "idCard1", IsActive = true, Name = "name1", Surname = "surname1" };
            var toCustomer = new CustomerEntity { Id = 2, IdCard = "idCard2", IsActive = true, Name = "name2", Surname = "surname2" };
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(fromCustomer.Id).Returns((CustomerEntity)null);
            repo.GetCustomer(toCustomer.Id).Returns(toCustomer);
            repo.TransferFunds(fromCustomer, toCustomer, 10).Returns(Result.Success);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.TransferFunds(1, 2, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Customer not found, id:1");
        }

        [TestMethod]
        public void GivenAccountManager_WhenCallingTransferFundsWithNonExistingReceiver_ThenProperResultShouldBeReceived()
        {
            // Arrange
            var fromCustomer = new CustomerEntity { Id = 1, IdCard = "idCard1", IsActive = true, Name = "name1", Surname = "surname1" };
            var toCustomer = new CustomerEntity { Id = 2, IdCard = "idCard2", IsActive = true, Name = "name2", Surname = "surname2" };
            var repo = Substitute.For<IRepository>();
            repo.GetCustomer(fromCustomer.Id).Returns(fromCustomer);
            repo.GetCustomer(toCustomer.Id).Returns((CustomerEntity)null);
            repo.TransferFunds(fromCustomer, toCustomer, 10).Returns(Result.Success);
            var manager = new AccountManager(repo, new TransferLocker());

            // Act
            var result = manager.TransferFunds(1, 2, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Customer not found, id:2");
        }
    }
}
