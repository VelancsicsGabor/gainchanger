﻿using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using TechnicalTest.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.Controller
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CustomerControllerTests
    {
        [TestMethod]
        public void GivenCustomerController_WhenCallingConstructorWithNullCustomerManager_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new CustomersController(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customersManager')");
        }

        [TestMethod]
        public void GivenCustomerController_WhenDeleteWithNonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(1).Returns(false);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Delete(1);

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenCustomerController_WhenDeleteWithExistingCustomer_ThenNoContentResultShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(1).Returns(true);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Delete(1);

            // Assert
            result.GetType().Should().Be(typeof(NoContentResult));
            customersManager.Received().DeleteCustomer(1);
        }

        [TestMethod]
        public void GivenCustomerController_WhenGettingAllCustomers_ThenOkObjectResultShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            var list = new List<CustomerDto>
            {
                new CustomerDto(), new CustomerDto(), new CustomerDto()
            };
            customersManager.GetCustomers().Returns(list);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Get().Result;

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(list);
        }

        [TestMethod]
        public void GivenCustomerController_WhenGetWithNonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.GetCustomer(1).Returns((CustomerDto)null);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Get(1).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenCustomerController_WhenGetWithExistingCustomer_ThenOkObjectResultShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            var customer = new CustomerDto();
            customersManager.GetCustomer(1).Returns(customer);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Get(1).Result;

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(customer);
        }

        [TestMethod]
        public void GivenCustomerController_WhenAddingACustomer_ThenCreatedShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customerRequest = new CustomerCreateRequestDto { IdCard = "idCard", Name = "name", Surname = "surname" };

            customersManager.Add(customerRequest).Returns(customer);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Post(customerRequest).Result;

            // Assert
            result.GetType().Should().Be(typeof(CreatedResult));
            (result as CreatedResult)?.Value.Should().BeEquivalentTo(customer);
            customersManager.Received().Add(customerRequest);
        }

        [TestMethod]
        public void GivenCustomerController_WhenUpdateANonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(false);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Put(customer).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenCustomerController_WhenUpdateAnExistingCustomer_ThenOkObjectResultShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            customersManager.Update(customer).Returns(customer);
            var controller = new CustomersController(customersManager);

            // Act
            var result = controller.Put(customer).Result;

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().Be(customer);
            customersManager.Received().Update(customer);
        }
    }
}