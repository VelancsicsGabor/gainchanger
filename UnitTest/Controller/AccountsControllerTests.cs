﻿using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using TechnicalTest.Controllers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.Controller
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AccountsControllerTests
    {
        [TestMethod]
        public void GivenAccountsController_WhenCallingConstructorWithNullCustomerManager_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new AccountsController(null, null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customersManager')");
        }

        [TestMethod]
        public void GivenAccountsController_WhenCallingConstructorWithNullAccountManager_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new AccountsController(Substitute.For<ICustomersManager>(), null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'accountManager')");
        }

        [TestMethod]
        public void GivenAccountsController_WhenGetWithNonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(1).Returns(false);
            var controller = new AccountsController(customersManager, Substitute.For<IAccountManager>());

            // Act
            var result = controller.Get(1).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenAccountsController_WhenGetWithExistingCustomer_ThenOkShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var fundResponse = new FundResponseDto { Id = customer.Id, Funds = 10 };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.GetAvailableFunds(customer.Id).Returns(fundResponse.Funds);
            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Get(1).Result;

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(fundResponse);
        }

        [TestMethod]
        public void GivenAccountsController_WhenDepositNonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var controller = new AccountsController(Substitute.For<ICustomersManager>(), Substitute.For<IAccountManager>());

            // Act
            var result = controller.Deposit(1, new FundRequestDto()).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenAccountsController_WhenDepositWithExistingCustomer_ThenOkShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var fundResponse = new FundResponseDto { Id = customer.Id, Funds = funds };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            customersManager.GetCustomer(customer.Id).Returns(customer);
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.DepositFunds(customer.Id, funds).Returns(Result.Success);
            accountManager.GetAvailableFunds(customer.Id).Returns(fundResponse.Funds + funds);
            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Deposit(1, new FundRequestDto { Funds = funds }).Result;

            // Assert
            fundResponse.Funds += funds;
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(fundResponse);
        }

        //This should never happen
        [TestMethod]
        public void GivenAccountsController_WhenDepositWithExistingCustomerAndCustomerManagerError_ThenBadRequestObjectResultShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var fundResponse = new FundResponseDto { Id = customer.Id, Funds = funds };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.DepositFunds(customer.Id, funds).Returns(Result.Failed("message"));
            accountManager.GetAvailableFunds(customer.Id).Returns(fundResponse.Funds + funds);
            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Deposit(1, new FundRequestDto { Funds = funds }).Result;

            // Assert
            fundResponse.Funds += funds;
            result.GetType().Should().Be(typeof(BadRequestObjectResult));
            (result as BadRequestObjectResult)?.Value.Should().BeEquivalentTo("message");
        }

        [TestMethod]
        public void GivenAccountsController_WhenWithdrawNonExistingCustomer_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var controller = new AccountsController(Substitute.For<ICustomersManager>(), Substitute.For<IAccountManager>());

            // Act
            var result = controller.Withdraw(1, new FundRequestDto()).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenAccountsController_WhenWithdrawWithExistingCustomer_ThenOkShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var fundResponse = new FundResponseDto { Id = customer.Id, Funds = funds };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            customersManager.GetCustomer(customer.Id).Returns(customer);
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.WithdrawFunds(customer.Id, funds).Returns(Result.Success);
            accountManager.GetAvailableFunds(customer.Id).Returns(fundResponse.Funds - funds);
            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Withdraw(1, new FundRequestDto { Funds = funds }).Result;

            // Assert
            fundResponse.Funds -= funds;
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(fundResponse);
        }

        [TestMethod]
        public void GivenAccountsController_WhenWithdrawWithError_ThenBadRequestObjectResultShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var fundResponse = new FundResponseDto { Id = customer.Id, Funds = funds };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            customersManager.GetCustomer(customer.Id).Returns(customer);
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.WithdrawFunds(customer.Id, funds).Returns(Result.Failed("message"));
            accountManager.GetAvailableFunds(customer.Id).Returns(fundResponse.Funds - funds);
            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Withdraw(1, new FundRequestDto { Funds = funds }).Result;

            // Assert
            fundResponse.Funds -= funds;
            result.GetType().Should().Be(typeof(BadRequestObjectResult));
            (result as BadRequestObjectResult)?.Value.Should().BeEquivalentTo("message");
        }

        [TestMethod]
        public void GivenAccountsController_WhenTransferNonExistingSender_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var controller = new AccountsController(Substitute.For<ICustomersManager>(), Substitute.For<IAccountManager>());

            // Act
            var result = controller.Transfer(new TransferRequestDto { From = 1, To = 2, Funds = 10 }).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(1);
        }

        [TestMethod]
        public void GivenAccountsController_WhenTransferNonExistingReceiver_ThenNotFoundShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer.Id).Returns(true);
            customersManager.GetCustomer(customer.Id).Returns(customer);
            var controller = new AccountsController(customersManager, Substitute.For<IAccountManager>());

            // Act
            var result = controller.Transfer(new TransferRequestDto { From = 1, To = 2, Funds = 10 }).Result;

            // Assert
            result.GetType().Should().Be(typeof(NotFoundObjectResult));
            (result as NotFoundObjectResult)?.Value.Should().Be(2);
        }

        [TestMethod]
        public void GivenAccountsController_WhenTransferWithError_ThenBadRequestShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer1 = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customer2 = new CustomerDto { Id = 2, IdCard = "idCard", Name = "name", Surname = "surname" };
            
            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer1.Id).Returns(true);
            customersManager.GetCustomer(customer1.Id).Returns(customer1);

            customersManager.DoesCustomerExists(customer2.Id).Returns(true);
            customersManager.GetCustomer(customer2.Id).Returns(customer2);

            var accountManager = Substitute.For<IAccountManager>();
            accountManager.TransferFunds(customer1.Id, customer2.Id, funds).Returns(Result.Failed("message"));

            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Transfer(new TransferRequestDto { From = 1, To = 2, Funds = 10 }).Result;

            // Assert
            result.GetType().Should().Be(typeof(BadRequestObjectResult));
            (result as BadRequestObjectResult)?.Value.Should().Be("message");
        }

        [TestMethod]
        public void GivenAccountsController_WhenTransfer_ThenBadRequestShouldBeReceived()
        {
            // Arrange
            var funds = 10;
            var customer1 = new CustomerDto { Id = 1, IdCard = "idCard", Name = "name", Surname = "surname" };
            var customer2 = new CustomerDto { Id = 2, IdCard = "idCard", Name = "name", Surname = "surname" };

            var customersManager = Substitute.For<ICustomersManager>();
            customersManager.DoesCustomerExists(customer1.Id).Returns(true);
            customersManager.GetCustomer(customer1.Id).Returns(customer1);

            customersManager.DoesCustomerExists(customer2.Id).Returns(true);
            customersManager.GetCustomer(customer2.Id).Returns(customer2);

            var accountManager = Substitute.For<IAccountManager>();
            accountManager.TransferFunds(customer1.Id, customer2.Id, funds).Returns(Result.Success);
            accountManager.GetAvailableFunds(customer1.Id).Returns(10);
            accountManager.GetAvailableFunds(customer2.Id).Returns(20);

            var controller = new AccountsController(customersManager, accountManager);

            // Act
            var result = controller.Transfer(new TransferRequestDto { From = 1, To = 2, Funds = 10 }).Result;

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            (result as OkObjectResult)?.Value.Should().BeEquivalentTo(new TransferResponseDto
            {
                From = customer1.Id, FromFunds = 10, To = customer2.Id, ToFunds = 20
            });
        }
    }
}
