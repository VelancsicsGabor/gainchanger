﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository.EntityModels;
using Repository.InMemory;

namespace UnitTest.Repository
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class InMemoryRepositoryTests
    {
        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingDelete_ThenCustomerIsActiveShouldBeSetToFalse()
        {
            // Arrange
            var customer = new CustomerEntity {IsActive = true};
            var repo = new InMemoryRepository();

            // Act
            repo.DeleteCustomer(customer);

            // Assert
            customer.IsActive.Should().BeFalse();
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingDoesCustomerExistForExistingCustomer_ThenTrueShouldReturn()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, IsActive = true };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);

            // Act
            var result = repo.DoesCustomerExist(1);

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingDoesCustomerExistForNonExistingCustomer_ThenTrueShouldReturn()
        {
            // Arrange
            var repo = new InMemoryRepository();
            
            // Act
            var result = repo.DoesCustomerExist(1);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingUpdateCustomerWithNullCustomerEntity_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().UpdateCustomer(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingUpdate_ThenItShouldBeUpdated()
        {
            // Arrange
            var customer = new CustomerEntity {Id = 1};
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            var updated = new CustomerEntity {Id = 1, IdCard = "idCard", Name = "name", Surname = "surname"};
            
            // Act
            repo.UpdateCustomer(updated);

            // Assert
            var result = repo.GetCustomer(updated.Id);
            result.Should().BeEquivalentTo(updated);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingDepositWithNullCustomerEntity_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().DepositFunds(null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingDeposit_ThenItShouldBeUpdated()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1 };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            
            // Act
            repo.DepositFunds(customer, 10);

            // Assert
            var result = repo.GetCustomer(customer.Id);
            result.Account.Should().NotBeNull();
            result.Account.Funds.Should().Be(10);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenGetAvailableFunds_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity {Funds = 10}};
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            
            // Act
            var result = repo.GetAvailableFunds(customer.Id);

            // Assert
            result.Should().Be(10);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenGetAvailableFundsWithoutFunds_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity()  };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            
            // Act
            var result = repo.GetAvailableFunds(customer.Id);

            // Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenGetAvailableFundsWithoutAccount_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1 };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            
            // Act
            var result = repo.GetAvailableFunds(customer.Id);

            // Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenGetAvailableFundsWithoutCustomer_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var repo = new InMemoryRepository();

            // Act
            var result = repo.GetAvailableFunds(1);

            // Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenGettingCustomers_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var customer1 = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = 10 } };
            var customer2 = new CustomerEntity { Id = 2, Account = new AccountEntity { Funds = 10 } };
            var customer3 = new CustomerEntity { Id = 3, Account = new AccountEntity { Funds = 10 } };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer1);
            repo.AddCustomer(customer2);
            repo.AddCustomer(customer3);

            // Act
            var result = repo.GetCustomers().ToList();

            // Assert
            result.Count().Should().Be(3);
            result.Should().ContainEquivalentOf(customer1);
            result.Should().ContainEquivalentOf(customer2);
            result.Should().ContainEquivalentOf(customer3);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenAddingExistingCustomers_ThenNullShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = 10 } };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);
            
            // Act
            var result = repo.AddCustomer(customer);

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenAddingCustomers_ThenProperResponseShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = 10 } };
            var repo = new InMemoryRepository();
            
            // Act
            var result = repo.AddCustomer(customer);

            // Assert
            result.Should().BeEquivalentTo(customer);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingAddWithNullCustomerEntity_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().AddCustomer(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingWithdrawWithNullCustomerEntity_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().WithdrawFunds(null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingWithdrawWithInsufficientFunds_ThenErrorShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1 , Account = new AccountEntity { Funds = 0 }};
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);

            // Act
            var result = repo.WithdrawFunds(customer, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Insufficient founds.");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingWithdrawWithNullAccount_ThenErrorShouldBeReceived()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1 };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);

            // Act
            var result = repo.WithdrawFunds(customer, 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Insufficient founds.");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingWithdraw_ThenItShouldBePersisted()
        {
            // Arrange
            var funds = 10;
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = funds } };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer);

            // Act
            var result = repo.WithdrawFunds(customer, funds);

            // Assert
            result.IsSuccess.Should().BeTrue();
            customer.Account.Funds.Should().Be(0);
            customer.Account.Transactions.Count.Should().Be(1);
            customer.Account.Transactions[0].BalanceChange.Should().Be(-funds);
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingTransferFundsWithNullSender_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().TransferFunds(null, null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'senderCustomerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingTransferFundsWithNullReceiver_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            Action act = () => new InMemoryRepository().TransferFunds(new CustomerEntity(), null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'receiverCustomerEntity')");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingTransferFundsWithInsufficientReceiver_ThenItShouldFail()
        {
            // Arrange
            var customer1 = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = 10 } };
            var customer2 = new CustomerEntity { Id = 2, Account = new AccountEntity { Funds = 10 } };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer1);
            repo.AddCustomer(customer2);

            // Act
            var result = repo.TransferFunds(customer1, customer2, 20);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Insufficient funds for transmitter.");
        }

        [TestMethod]
        public void GivenInMemoryRepository_WhenCallingTransferFunds_ThenItShouldBePersisted()
        {
            // Arrange
            var customer1 = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = 10 } };
            var customer2 = new CustomerEntity { Id = 2, Account = new AccountEntity { Funds = 10 } };
            var repo = new InMemoryRepository();
            repo.AddCustomer(customer1);
            repo.AddCustomer(customer2);

            // Act
            var result = repo.TransferFunds(customer1, customer2, 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
            customer1.Account.Should().NotBeNull();
            customer1.Account.Funds.Should().Be(0);
            customer1.Account.Transactions.Count.Should().Be(1);
            customer1.Account.Transactions[0].BalanceChange.Should().Be(-10);
            customer2.Account.Should().NotBeNull();
            customer2.Account.Funds.Should().Be(20);
            customer2.Account.Transactions.Count.Should().Be(1);
            customer2.Account.Transactions[0].BalanceChange.Should().Be(10);
        }
    }
}
