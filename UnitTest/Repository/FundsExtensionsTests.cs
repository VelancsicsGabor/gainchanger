﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository.EntityModels;
using Repository.Extensions;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.Repository
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class FundsExtensionsTests
    {
        [TestMethod]
        public void GivenCustomerEntity_WhenCallingDepositWithNullAccount_ThenFundAndTransactionShouldAdded()
        {
            // Arrange
            var customer = new CustomerEntity { Id = 1 };
            var fund = 10;

            // Act
            customer.Deposit(fund);

            // Assert
            customer.Account.Should().NotBeNull();
            customer.Account.Funds.Should().Be(fund);
            customer.Account.Transactions.Count.Should().Be(1);
            customer.Account.Transactions[0].BalanceChange.Should().Be(fund);
        }

        [TestMethod]
        public void GivenCustomerEntity_WhenCallingDepositWithAccount_ThenFundAndTransactionShouldAdded()
        {
            // Arrange
            int originalFunds = 10;
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = originalFunds } };
            var fund = 10;

            // Act
            customer.Deposit(fund);

            // Assert
            customer.Account.Should().NotBeNull();
            customer.Account.Funds.Should().Be(originalFunds + fund);
            customer.Account.Transactions.Count.Should().Be(1);
            customer.Account.Transactions[0].BalanceChange.Should().Be(fund);
        }

        [DataTestMethod]
        [DataRow(10, 10, true, DisplayName = "10 in account, 10 withdraw, expected true")]
        [DataRow(10, 9, true, DisplayName = "10 in account, 9 withdraw, expected true")]
        [DataRow(10, 11, false, DisplayName = "10 in account, 11 withdraw, expected false")]
        public void GivenAccountEntity_WhenCalledCanWithdrawWithAccount_ThenExpectedShouldBeReceived(int fundInAccount, int withdraw, bool expected)
        {
            // Arrange
            var account = new AccountEntity {Funds = fundInAccount};
            
            // Act
            var result = account.CanWithdraw(withdraw);

            // Assert
            result.Should().Be(expected);
        }

        [TestMethod]
        public void GivenAccountEntity_WhenCalledCanWithdrawWithNullAccount_ThenFalseShouldBeReceived()
        {
            // Act
            var result = ((AccountEntity)null).CanWithdraw(10);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void GivenAccountEntity_WhenCalledWithdrawWithAccountFewerFund_ThenNothingShouldChange()
        {
            var originalFunds = 10;
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity {Funds = originalFunds } };
            var fund = 11;

            // Act
            customer.Withdraw(fund);

            // Assert
            customer.Account.Funds.Should().Be(originalFunds);
            customer.Account.Transactions.Count.Should().Be(0);
        }

        [TestMethod]
        public void GivenAccountEntity_WhenCalledWithdrawWithAccountHigherFund_ThenFundShouldChangeAndTransactionShouldBeCreated()
        {
            var originalFunds = 10;
            var customer = new CustomerEntity { Id = 1, Account = new AccountEntity { Funds = originalFunds } };
            var fund = 10;

            // Act
            customer.Withdraw(fund);

            // Assert
            customer.Account.Funds.Should().Be(originalFunds - fund);
            customer.Account.Transactions.Count.Should().Be(1);
            customer.Account.Transactions[0].BalanceChange.Should().Be(-fund);
        }
    }
}
