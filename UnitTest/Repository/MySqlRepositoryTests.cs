﻿using System.Linq;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Repository.EntityModels;
using Repository.Interface;
using Repository.Mysql;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace UnitTest.Repository
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class MySqlRepositoryTests
    {
        [TestMethod]
        public void GivenMysqlRepository_WhenCallingConstructorWithNullDbContext_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Act
            // ReSharper disable once ObjectCreationAsStatement
            Action act = () => new MysqlRepository(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'dbContext')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingDeleteCustomerWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.DeleteCustomer(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingDelete_ThenIsActiveShouldBeSetToFalse()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
                new CustomerEntity {Id = 2, IsActive = false}
            };
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            repo.DeleteCustomer(data[0]);

            // Assert
            data[0].IsActive.Should().BeFalse();
            context.Received().SaveChanges();
        }

        [DataTestMethod]
        [DataRow(1, true, DisplayName = "Existing active customer")]
        [DataRow(2, false, DisplayName = "Existing non active customer")]
        [DataRow(3, false, DisplayName = "None existing customer")]
        public void GivenMysqlRepository_WhenCallingDoesCustomerExist_ThenProperObjectShouldReturn(int id, bool expected)
        {
            // Arrange
            var data = new List<CustomerEntity>
                {
                    new CustomerEntity {Id = 1, IsActive = true},
                    new CustomerEntity {Id = 2, IsActive = false}
                };
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.DoesCustomerExist(id);

            // Assert
            result.Should().Be(expected);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingGetCustomers_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
                new CustomerEntity {Id = 2, IsActive = false},
                new CustomerEntity {Id = 3, IsActive = true}
            };
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.GetCustomers().ToList();

            // Assert
            result.Should().Contain(x => x.Id == data[0].Id);
            result.Should().NotContain(x => x.Id == data[1].Id);
            result.Should().Contain(x => x.Id == data[2].Id);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingAddCustomerWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.AddCustomer(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenAddingCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
                new CustomerEntity {Id = 2, IsActive = false}
            };
            var customer = new CustomerEntity {Id = 3, IsActive = true};
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.AddCustomer(customer);

            // Assert
            result.Should().Be(customer);
            dbSet.Received().Add(customer);
            context.Received().SaveChanges();
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenGettingCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
                new CustomerEntity {Id = 2, IsActive = false}
            };
            
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
           
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.GetCustomer(1);

            // Assert
            result.Should().BeEquivalentTo(data[0]);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingUpdateCustomerWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.UpdateCustomer(null);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenUpdatingCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
                new CustomerEntity {Id = 2, IsActive = false}
            };
            
            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);
            var customer = new CustomerEntity{ Id = 1, IsActive = false };

            // Act
            repo.UpdateCustomer(customer);

            // Assert
            dbSet.Received().Update(customer);
            context.Received().SaveChanges();
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenGettingAvailableFunds_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true, Account = new AccountEntity{ Funds = 50 }},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);
            
            // Act
            var result = repo.GetAvailableFunds(data[0].Id);

            // Assert
            result.Should().Be(data[0].Account.Funds);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenGettingAvailableFundsForNonExistingCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true, Account = new AccountEntity{ Funds = 50 }},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);
            
            // Act
            var result = repo.GetAvailableFunds(2);

            // Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenGettingAvailableFundsForNonAccountCustomer_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.GetAvailableFunds(1);

            // Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingDepositFundsWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.DepositFunds(null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenDepositFunds_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);
            
            // Act
            var result = repo.DepositFunds(data[0], 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
            context.Received().SaveChanges();
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingWithdrawFundsWithNullCustomer_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.WithdrawFunds(null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'customerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenWithdrawFundsWithInsufficientFundsAndNullAccount_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.WithdrawFunds(data[0], 10);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Insufficient founds.");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenWithdrawFunds_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true, Account = new AccountEntity{ Funds = 10 }},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.WithdrawFunds(data[0], 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
            context.Received().SaveChanges();
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingTransferFundsWithNullSender_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.TransferFunds(null, null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'senderCustomerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingTransferFundsWithNullReceiver_ThenArgumentNullExceptionShouldBeThrown()
        {
            // Arrange
            var repo = new MysqlRepository(Substitute.For<ITechnicalTestDbContext>());

            // Act
            Action act = () => repo.TransferFunds(new CustomerEntity(), null, 10);

            // Assert
            act.Should()
                .Throw<ArgumentNullException>()
                .WithMessage("Value cannot be null. (Parameter 'receiverCustomerEntity')");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingTransferFundsWithInsufficientFundForSender_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true, Account = new AccountEntity{ Funds = 10 }},
                new CustomerEntity {Id = 2, IsActive = true, Account = new AccountEntity{ Funds = 20 }},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.TransferFunds(data[0], data[1], 20);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Message.Should().Be("Insufficient funds for sender.");
        }

        [TestMethod]
        public void GivenMysqlRepository_WhenCallingTransferFunds_ThenProperObjectShouldReturn()
        {
            // Arrange
            var data = new List<CustomerEntity>
            {
                new CustomerEntity {Id = 1, IsActive = true, Account = new AccountEntity{ Funds = 10 }},
                new CustomerEntity {Id = 2, IsActive = true, Account = new AccountEntity{ Funds = 20 }},
            };

            var dbSet = GetDbSet(data);
            var context = Substitute.For<ITechnicalTestDbContext>();
            context.Set<CustomerEntity>().Returns(dbSet);
            var repo = new MysqlRepository(context);

            // Act
            var result = repo.TransferFunds(data[0], data[1], 10);

            // Assert
            result.IsSuccess.Should().BeTrue();
            context.Received().SaveChanges();
        }

        private DbSet<CustomerEntity> GetDbSet(List<CustomerEntity> data)
        {
            var mockDbSet = Substitute.For<DbSet<CustomerEntity>, IQueryable<CustomerEntity>>();
            var queryable = data.AsQueryable();
            ((IQueryable<CustomerEntity>)mockDbSet).Provider.Returns(queryable.Provider);
            ((IQueryable<CustomerEntity>)mockDbSet).Expression.Returns(queryable.Expression);
            ((IQueryable<CustomerEntity>)mockDbSet).ElementType.Returns(queryable.ElementType);
            ((IQueryable<CustomerEntity>)mockDbSet).GetEnumerator().Returns(queryable.GetEnumerator());

            return mockDbSet;
        }
    }
}
