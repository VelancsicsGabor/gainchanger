﻿namespace DataTransferObjects
{
    /// <summary>
    /// FundResponse DTO
    /// </summary>
    public class FundResponseDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the funds.
        /// </summary>
        /// <value>
        /// The funds.
        /// </value>
        public decimal Funds { get; set; }

        #endregion
    }
}