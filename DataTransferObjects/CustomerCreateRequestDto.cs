﻿namespace DataTransferObjects
{
    /// <summary>
    /// CustomerCreateRequest DTO
    /// </summary>
    public class CustomerCreateRequestDto
    {
        #region Fields
        
        /// <summary>
        /// Gets or sets the identifier card.
        /// </summary>
        /// <value>
        /// The identifier card.
        /// </value>
        public string IdCard { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        /// <value>
        /// The surname.
        /// </value>
        public string Surname { get; set; }

        #endregion
    }
}