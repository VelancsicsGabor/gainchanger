﻿namespace DataTransferObjects
{
    /// <summary>
    /// Result
    /// </summary>
    public class Result
    {
        /// <summary>
        /// IsSuccess
        /// </summary>
        public bool IsSuccess { get;  }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Private constructor for result
        /// </summary>
        /// <param name="message"></param>
        /// <param name="isSuccess"></param>
        private Result(string message, bool isSuccess)
        {
            Message = message;
            IsSuccess = isSuccess;
        }

        /// <summary>
        /// Success result.
        /// </summary>
        public static Result Success { get; } = new Result(string.Empty, true);
        
        /// <summary>
        /// Failed result
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>Result</returns>
        public static Result Failed(string message) =>
            new Result(message, false);
    }
}