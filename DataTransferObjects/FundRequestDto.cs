﻿using System.ComponentModel.DataAnnotations;

namespace DataTransferObjects
{
    /// <summary>
    /// FundRequest DTO
    /// </summary>
    public class FundRequestDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the funds.
        /// </summary>
        /// <value>
        /// The funds.
        /// </value>
        [Required]
        public decimal Funds { get; set; }

        #endregion
    }
}