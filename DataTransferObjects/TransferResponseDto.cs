﻿namespace DataTransferObjects
{
    /// <summary>
    /// TransferResponse DTO
    /// </summary>
    public class TransferResponseDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the sender customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public int From { get; set; }

        /// <summary>
        /// Gets or sets the sender's funds.
        /// </summary>
        /// <value>
        /// The sender's funds.
        /// </value>
        public decimal FromFunds { get; set; }

        /// <summary>
        /// Gets or sets the receiver customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public int To { get; set; }

        /// <summary>
        /// Gets or sets the receiver's funds.
        /// </summary>
        /// <value>
        /// The receiver's funds.
        /// </value>
        public decimal ToFunds { get; set; }

        #endregion
    }
}