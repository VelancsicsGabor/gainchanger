﻿using System.ComponentModel.DataAnnotations;

namespace DataTransferObjects
{
    /// <summary>
    /// TransferRequest DTO
    /// </summary>
    public class TransferRequestDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the sender customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        [Required]
        public int From { get; set; }

        /// <summary>
        /// Gets or sets the receiver customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        [Required]
        public int To { get; set; }

        /// <summary>
        /// Gets or sets the funds.
        /// </summary>
        /// <value>
        /// The funds.
        /// </value>
        [Required]
        public decimal Funds { get; set; }

        #endregion
    }
}