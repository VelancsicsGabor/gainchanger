﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.EntityModels
{
    [ExcludeFromCodeCoverage]
    public class AccountEntity
    {
        #region Fields

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the CustomerId.
        /// </summary>
        /// <value>
        /// The CustomerId.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Customer.
        /// </summary>
        /// <value>
        /// The Customer.
        /// </value>
        public CustomerEntity Customer { get; set; }

        /// <summary>
        /// Gets or sets the funds.
        /// </summary>
        /// <value>
        /// The funds.
        /// </value>
        public decimal Funds { get; set; }

        /// <summary>
        /// Gets or sets the Transactions.
        /// </summary>
        /// <value>
        /// The Transactions.
        /// </value>
        public List<AccountTransactionEntity> Transactions { get; set; } = new List<AccountTransactionEntity>();
        
        #endregion
    }

    [ExcludeFromCodeCoverage]
    public class AccountEntityConfiguration : IEntityTypeConfiguration<AccountEntity>
    {
        /// <inheritdoc />
        public virtual void Configure(EntityTypeBuilder<AccountEntity> builder)
        {
            builder.ToTable("Account");
            builder.HasKey(x => new { x.Id });
            builder.Property(x => x.Id)
                .IsRequired();
            builder.HasMany(x => x.Transactions)
                .WithOne(x => x.Account)
                .HasForeignKey(x => x.AccountId);
        }
    }
}