﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Diagnostics.CodeAnalysis;

namespace Repository.EntityModels
{
    /// <summary>
    /// Customer Entity
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class CustomerEntity
    {
        #region Fields

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the identifier card.
        /// </summary>
        /// <value>
        /// The identifier card.
        /// </value>
        public string IdCard { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        /// <value>
        /// The surname.
        /// </value>
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the isActive.
        /// </summary>
        /// <value>
        /// The IsActive.
        /// </value>
        public bool IsActive { get; set; } = true;
        
        /// <summary>
        /// Gets or sets the Account.
        /// </summary>
        /// <value>
        /// The Account.
        /// </value>
        public AccountEntity Account { get; set; }

        #endregion
    }

    [ExcludeFromCodeCoverage]
    public class CustomerEntityConfiguration : IEntityTypeConfiguration<CustomerEntity>
    {
        /// <inheritdoc />
        public virtual void Configure(EntityTypeBuilder<CustomerEntity> builder)
        {
            builder.ToTable("Customer");
            builder.HasKey(x => new { x.Id });
            builder.Property(x => x.Id)
                .IsRequired();
            builder.HasOne(x => x.Account)
                .WithOne(x => x.Customer)
                .HasForeignKey<AccountEntity>(x => x.CustomerId);
        }
    }
}
