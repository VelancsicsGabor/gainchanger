﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.EntityModels
{
    [ExcludeFromCodeCoverage]
    public class AccountTransactionEntity : IAuditEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the AccountId.
        /// </summary>
        /// <value>
        /// The AccountId.
        /// </value>
        public int AccountId { get; set; }

        /// <summary>
        /// Gets or sets the Account.
        /// </summary>
        /// <value>
        /// The Account.
        /// </value>
        public AccountEntity Account { get; set; }

        /// <summary>
        /// Gets or sets the BalanceChange.
        /// </summary>
        /// <value>
        /// The BalanceChange.
        /// </value>
        public decimal BalanceChange { get; set; }
        
        /// <summary>
        /// Gets or sets the Created.
        /// </summary>
        /// <value>
        /// The Created.
        /// </value>
        public DateTimeOffset Created { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class AccountTransactionEntityConfiguration : IEntityTypeConfiguration<AccountTransactionEntity>
    {
        /// <inheritdoc />
        public virtual void Configure(EntityTypeBuilder<AccountTransactionEntity> builder)
        {
            builder.ToTable("AccountTransaction");
            builder.HasKey(x => new { x.Id });
            builder.Property(x => x.Id)
                .IsRequired();
            builder.HasOne(x => x.Account)
                .WithMany(x => x.Transactions)
                .HasForeignKey(x => x.AccountId);
        }
    }
}