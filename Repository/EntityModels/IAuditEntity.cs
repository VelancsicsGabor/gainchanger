﻿using System;

namespace Repository.EntityModels
{
    /// <summary>
    /// Audit entity interface
    /// </summary>
    public interface IAuditEntity
    {
        public DateTimeOffset Created { get; set; }
    }
}