﻿using DataTransferObjects;
using Repository.EntityModels;
using Repository.Extensions;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.InMemory
{

    /// <summary>
    /// InMemoryRepository
    /// </summary>
    /// <seealso cref="IRepository" />
    public class InMemoryRepository : IRepository
    {
        /// <summary>
        /// The customers
        /// </summary>
        private readonly List<CustomerEntity> _customers;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryRepository"/> class.
        /// </summary>
        public InMemoryRepository()
        {
            _customers = new List<CustomerEntity>();
        }

        /// <summary>
        /// Deletes the customerDto.
        /// </summary>
        /// <param name="customerEntity"></param>
        public void DeleteCustomer(CustomerEntity customerEntity)
        {
            customerEntity.IsActive = false;
        }

        /// <summary>
        /// Check that the customer exists with the given id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns></returns>
        public bool DoesCustomerExist(int id)
        {
            return _customers.Any(c => c.Id == id && c.IsActive);
        }

        /// <summary>
        /// Updates teh customer.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        public void UpdateCustomer(CustomerEntity customerEntity)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            if (DoesCustomerExist(customerEntity.Id))
            {
                var existingCustomer = GetCustomer(customerEntity.Id);
                existingCustomer.IdCard = customerEntity.IdCard;
                existingCustomer.Name = customerEntity.Name;
                existingCustomer.Surname = customerEntity.Surname;
            }
        }

        /// <summary>
        /// Deposits the funds.
        /// </summary>
        /// <param name="customerEntity"></param>
        /// <param name="funds">The funds.</param>
        public Result DepositFunds(CustomerEntity customerEntity, decimal funds)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            customerEntity.Deposit(funds);
            return Result.Success;
        }

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customerDto identifier.</param>
        /// <returns></returns>
        public decimal GetAvailableFunds(int customerId)
        {
            var customer = GetCustomer(customerId);
            return customer?.Account?.Funds ?? 0;
        }

        /// <summary>
        /// Gets the customerDto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerEntity GetCustomer(int id)
        {
            return _customers.FirstOrDefault(c => c.Id == id && c.IsActive);
        }
        
        /// <summary>
        /// Gets all the active customers
        /// </summary>
        /// <returns>List of active customers</returns>
        public IEnumerable<CustomerEntity> GetCustomers()
        {
            return _customers.Where(x => x.IsActive);
        }

        /// <summary>
        /// Saves the customerDto.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        public CustomerEntity AddCustomer(CustomerEntity customerEntity)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            if (!DoesCustomerExist(customerEntity.Id))
            {
                customerEntity.Id = _customers.Any() ? _customers.Max(x => x.Id) + 1 : 1;
                _customers.Add(customerEntity);
                return customerEntity;
            }

            return null;
        }

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        /// <param name="funds">The funds.</param>
        /// /// <returns>Result</returns>
        public Result WithdrawFunds(CustomerEntity customerEntity, decimal funds)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            if (!customerEntity.Account.CanWithdraw(funds))
            {
                return Result.Failed("Insufficient founds.");
            }

            customerEntity.Withdraw(funds);
            return Result.Success;
        }
        
        /// <summary>
        /// Transfer the funds
        /// </summary>
        /// <param name="senderCustomerEntity">Transmitter customer.</param>
        /// <param name="receiverCustomerEntity">Receiver customer.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result TransferFunds(CustomerEntity senderCustomerEntity, CustomerEntity receiverCustomerEntity, decimal funds)
        {
            if (senderCustomerEntity == null) throw new ArgumentNullException(nameof(senderCustomerEntity));
            if (receiverCustomerEntity == null) throw new ArgumentNullException(nameof(receiverCustomerEntity));

            if (!senderCustomerEntity.Account.CanWithdraw(funds))
            {
                return Result.Failed("Insufficient funds for transmitter.");
            }

            receiverCustomerEntity.Deposit(funds);
            senderCustomerEntity.Withdraw(funds);
            
            return Result.Success;
        }
    }
}
