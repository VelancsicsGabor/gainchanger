﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository.InMemory;
using Repository.Interface;
using Repository.Mysql;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Repository
{
    [ExcludeFromCodeCoverage]
    public static class RepositoryService
    {
        private const string MariaDb = "mariadb";
        private const string InMemory = "inmemory";

        public static IServiceCollection AddRepositoryService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext(configuration);
            return services;
        }

        private static void AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var persistenceConfig = configuration.GetSection("Persistence");
            var type = persistenceConfig["Type"];
            var connectionString = persistenceConfig["ConnectionString"];
            switch (type)
            {
                case MariaDb:
                    services.AddMysql(connectionString);
                    break;
                case InMemory:
                    services.AddSingleton<IRepository, InMemoryRepository>();
                    break;
                default:
                    throw new ArgumentException("Persistence Configuration error. Didn't find the correct persistence type.");
            }
        }

        public static IServiceCollection AddMysql(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IRepository, MysqlRepository>();
            services.AddScoped<IModelCreator, MysqlModelCreator>();
            services.AddDbContext<ITechnicalTestDbContext, TechnicalTestDbContext>(options =>
            {
                options.UseMySQL(connectionString);
            });
            return services;
        }
    }
}
