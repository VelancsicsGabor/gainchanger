﻿using Microsoft.EntityFrameworkCore;
using Repository.Interface;
using Repository.EntityModels;
using System.Diagnostics.CodeAnalysis;

namespace Repository.Mysql
{
    [ExcludeFromCodeCoverage]
    public class MysqlModelCreator : IModelCreator
    {
        /// <inheritdoc />
        public void Create(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerEntityConfiguration());
            modelBuilder.ApplyConfiguration(new AccountEntityConfiguration());
            modelBuilder.ApplyConfiguration(new AccountTransactionEntityConfiguration());
        }
    }
}