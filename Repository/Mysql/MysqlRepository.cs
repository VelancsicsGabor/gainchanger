﻿using DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Repository.EntityModels;
using Repository.Extensions;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Mysql
{
    public class MysqlRepository : IRepository
    {
        private readonly ITechnicalTestDbContext _dbContext;
        private readonly DbSet<CustomerEntity> _customer;

        /// <summary>
        /// Constructor for MysqlRepository
        /// </summary>
        /// <param name="dbContext">The DbContext.</param>
        public MysqlRepository(ITechnicalTestDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _customer = _dbContext.Set<CustomerEntity>();
        }

        /// <summary>
        /// Deletes the customerEntity.
        /// </summary>
        /// <param name="customerEntity"></param>
        public void DeleteCustomer(CustomerEntity customerEntity)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            customerEntity.IsActive = false;
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Check that the customer exists with the given id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns></returns>
        public bool DoesCustomerExist(int id)
        {
            return _customer.FirstOrDefault(x => x.Id == id && x.IsActive) != null;
        }

        /// <summary>
        /// Gets the customerEntity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerEntity GetCustomer(int id)
        {
            var entity = _customer.Include(x => x.Account)
                .FirstOrDefault(x => x.Id == id && x.IsActive);

            return entity;
        }

        /// <summary>
        /// Gets all the active customers
        /// </summary>
        /// <returns>List of active customers</returns>
        public IEnumerable<CustomerEntity> GetCustomers()
        {
            return _customer.Include(x => x.Account)
                .Where(x => x.IsActive)
                .ToList();
        }

        /// <summary>
        /// Saves the customerEntity.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        public CustomerEntity AddCustomer(CustomerEntity customerEntity)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            _customer.Add(customerEntity);
            _dbContext.SaveChanges();
            return customerEntity;
        }

        /// <summary>
        /// Updates teh customer.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        public void UpdateCustomer(CustomerEntity customerEntity)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));
            
            _customer.Update(customerEntity);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Deposits the funds.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result DepositFunds(CustomerEntity customerEntity, decimal funds)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            customerEntity.Deposit(funds);

            _dbContext.SaveChanges();
            return Result.Success;
        }

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customerEntity identifier.</param>
        /// <returns>The funds.</returns>
        public decimal GetAvailableFunds(int customerId)
        {
            var customer = GetCustomer(customerId);
            return customer?.Account?.Funds ?? 0;
        }

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result WithdrawFunds(CustomerEntity customerEntity, decimal funds)
        {
            if (customerEntity == null) throw new ArgumentNullException(nameof(customerEntity));

            if (!customerEntity.Account.CanWithdraw(funds))
            {
                return Result.Failed("Insufficient founds.");
            }

            customerEntity.Withdraw(funds);

            _dbContext.SaveChanges();
            return Result.Success;
        }

        /// <summary>
        /// Transfer the funds
        /// </summary>
        /// <param name="senderCustomerEntity">Transmitter customer.</param>
        /// <param name="receiverCustomerEntity">Receiver customer.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result TransferFunds(CustomerEntity senderCustomerEntity, CustomerEntity receiverCustomerEntity, decimal funds)
        {
            if (senderCustomerEntity == null) throw new ArgumentNullException(nameof(senderCustomerEntity));
            if (receiverCustomerEntity == null) throw new ArgumentNullException(nameof(receiverCustomerEntity));

            if (!senderCustomerEntity.Account.CanWithdraw(funds))
            {
                return Result.Failed("Insufficient funds for sender.");
            }

            receiverCustomerEntity.Deposit(funds);
            senderCustomerEntity.Withdraw(funds);

            _dbContext.SaveChanges();
            return Result.Success;
        }
    }
}
