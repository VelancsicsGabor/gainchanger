﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Repository.Mysql;

namespace Repository
{
    [ExcludeFromCodeCoverage]
    // ReSharper disable once UnusedMember.Global
    public class TechnicalTestDbContextFactory : IDesignTimeDbContextFactory<TechnicalTestDbContext>
    {
        /// <summary>
        /// Design time migration - creation of db context
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TechnicalTestDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TechnicalTestDbContext>();
            
            optionsBuilder.UseMySQL("connectionString",
                b => b.MigrationsAssembly("Repository"));
            
            return new TechnicalTestDbContext(optionsBuilder.Options, new MysqlModelCreator());
        }
    }
}