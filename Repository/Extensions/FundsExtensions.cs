﻿using Repository.EntityModels;

namespace Repository.Extensions
{
    public static class FundsExtensions
    {
        /// <summary>
        /// Deposit extension for customerEntity.
        /// </summary>
        /// <param name="customerEntity">The customer.</param>
        /// <param name="funds">The funds.</param>
        public static void Deposit(this CustomerEntity customerEntity, decimal funds)
        {
            if (customerEntity.Account == null)
            {
                customerEntity.Account = new AccountEntity { Customer = customerEntity, Funds = funds };
                customerEntity.Account.Transactions.Add(new AccountTransactionEntity { BalanceChange = funds });
            }
            else
            {
                customerEntity.Account.Funds += funds;
                customerEntity.Account.Transactions.Add(new AccountTransactionEntity { BalanceChange = funds });
            }
        }

        /// <summary>
        /// Withdraw extension for customerEntity.
        /// </summary>
        /// <param name="customerEntity">The customer.</param>
        /// <param name="funds">The funds.</param>
        public static void Withdraw(this CustomerEntity customerEntity, decimal funds)
        {
            if (!customerEntity.Account.CanWithdraw(funds))
            {
                return;
            }

            customerEntity.Account.Funds -= funds;
            customerEntity.Account.Transactions.Add(new AccountTransactionEntity {BalanceChange = -funds});
        }

        /// <summary>
        /// Helper extension for account to decide that enough funds are available for withdraw.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="funds">The funds.</param>
        /// <returns></returns>
        public static bool CanWithdraw(this AccountEntity account, decimal funds)
        {
            return account != null && account.Funds - funds >= 0;
        }
    }
}