﻿using Repository.EntityModels;
using Microsoft.EntityFrameworkCore;
using Repository.Interface;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Repository
{
    [ExcludeFromCodeCoverage]

    public class TechnicalTestDbContext : DbContext, ITechnicalTestDbContext
    {

        private readonly IModelCreator _modelCreator;

        /// <summary>
        /// Constructor for MysqlRepository
        /// </summary>
        /// <param name="options">The DbContextOptions.</param>
        /// <param name="modelCreator">The ModelCreator.</param>
        public TechnicalTestDbContext(DbContextOptions<TechnicalTestDbContext> options, IModelCreator modelCreator)
            : base(options)
        {
            _modelCreator = modelCreator ?? throw new ArgumentNullException(nameof(modelCreator));
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _modelCreator.Create(modelBuilder);
        }

        /// <summary>
        /// TechnicalTestDbContext SaveChanges override
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            SetAuditProperties();

            return base.SaveChanges();
        }

        private void SetAuditProperties()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is IAuditEntity && e.State == EntityState.Added);

            var now = DateTimeOffset.UtcNow;

            foreach (var entity in entries)
            {
                var auditEntity = (IAuditEntity)entity.Entity;

                if (entity.State == EntityState.Added)
                {
                    auditEntity.Created = now;
                }
            }
        }
    }
}
