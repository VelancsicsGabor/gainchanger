﻿using Microsoft.EntityFrameworkCore;

namespace Repository.Interface
{
    /// <summary>
    /// TechnicalTestDbContext interface
    /// </summary>
    public interface ITechnicalTestDbContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        int SaveChanges();
    }
}