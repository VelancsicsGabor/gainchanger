﻿using DataTransferObjects;
using Repository.EntityModels;
using System.Collections.Generic;

namespace Repository.Interface
{
    /// <summary>
    /// Repository interface
    /// </summary>
    public interface IRepository
    {
        #region Methods

        /// <summary>
        /// Deletes the customerEntity.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        void DeleteCustomer(CustomerEntity customerEntity);

        /// <summary>
        /// Check that the customer exists with the given id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns></returns>
        bool DoesCustomerExist(int id);

        /// <summary>
        /// Gets the customerEntity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerEntity GetCustomer(int id);

        /// <summary>
        /// Gets all the active customers
        /// </summary>
        /// <returns>List of active customers</returns>
        IEnumerable<CustomerEntity> GetCustomers();

        /// <summary>
        /// Saves the customerEntity.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        CustomerEntity AddCustomer(CustomerEntity customerEntity);

        /// <summary>
        /// Updates teh customer.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        void UpdateCustomer(CustomerEntity customerEntity);

        /// <summary>
        /// Deposits the funds.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        Result DepositFunds(CustomerEntity customerEntity, decimal funds);

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customerEntity identifier.</param>
        /// <returns>The funds.</returns>
        decimal GetAvailableFunds(int customerId);

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="customerEntity">The customerEntity.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        Result WithdrawFunds(CustomerEntity customerEntity, decimal funds);

        /// <summary>
        /// Transfer the funds
        /// </summary>
        /// <param name="senderCustomerEntity">Transmitter customer.</param>
        /// <param name="receiverCustomerEntity">Receiver customer.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        Result TransferFunds(CustomerEntity senderCustomerEntity, CustomerEntity receiverCustomerEntity, decimal funds);

        #endregion
    }
}