﻿using Microsoft.EntityFrameworkCore;

namespace Repository.Interface
{
    /// <summary>
    /// Model creator interface.
    /// </summary>
    public interface IModelCreator
    {
        void Create(ModelBuilder modelBuilder);
    }
}