﻿using AutoMapper;
using DataTransferObjects;
using Repository.EntityModels;

namespace BusinessLogic.MappingProfiles
{
    public class DtoEntityProfile : Profile
    {
        /// <summary>
        /// Constructor for mapping profile.
        /// </summary>
        public DtoEntityProfile()
        {
            CreateMap<CustomerEntity, CustomerDto>();
            CreateMap<CustomerDto, CustomerEntity>();

            CreateMap<CustomerCreateRequestDto, CustomerEntity>();
        }
    }
}