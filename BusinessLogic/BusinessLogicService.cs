﻿using BusinessLogic.Manager;
using BusinessLogic.Manager.Interface;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;
using BusinessLogic.MappingProfiles;

namespace BusinessLogic
{
    [ExcludeFromCodeCoverage]
    public static class BusinessLogicService
    {
        /// <summary>
        /// Add BusinessLogic Services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddBusinessLogicService(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(DtoEntityProfile));
            services.AddScoped<ICustomersManager, CustomersManager>();
            services.AddScoped<IAccountManager, AccountManager>();
            services.AddSingleton(new TransferLocker());
            return services;
        }
    }
}
