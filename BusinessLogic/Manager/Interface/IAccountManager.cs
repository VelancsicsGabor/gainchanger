﻿using DataTransferObjects;

namespace BusinessLogic.Manager.Interface
{
    /// <summary>
    /// AccountManager interface
    /// </summary>
    public interface IAccountManager
    {
        #region Methods

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customerEntity identifier.</param>
        /// <returns>The funds.</returns>
        decimal GetAvailableFunds(int customerId);

        /// <summary>
        /// Deposit the funds.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>The Result.</returns>
        Result DepositFunds(int id, decimal funds);

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        Result WithdrawFunds(int id, decimal funds);

        /// <summary>
        /// Transfer the funds
        /// </summary>
        /// <param name="fromId">The from customer id.</param>
        /// <param name="toId">The to customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        Result TransferFunds(int fromId, int toId, decimal funds);

        #endregion
    }
}