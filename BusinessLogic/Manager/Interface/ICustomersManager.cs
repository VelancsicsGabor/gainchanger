﻿using DataTransferObjects;
using System.Collections.Generic;

namespace BusinessLogic.Manager.Interface
{
    /// <summary>
    /// CustomersManager interface
    /// </summary>
    public interface ICustomersManager
    {
        #region Methods

        /// <summary>
        /// Deletes the customerDto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomer(int id);

        /// <summary>
        /// Gets the customerDto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerDto GetCustomer(int id);

        /// <summary>
        /// Gets all the active customers
        /// </summary>
        /// <returns>List of active customers</returns>
        IEnumerable<CustomerDto> GetCustomers();

        /// <summary>
        /// Check that the customer exists with the given id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns></returns>
        bool DoesCustomerExists(int id);

        /// <summary>
        /// Add new customer
        /// </summary>
        /// <param name="customerDto">The request.</param>
        /// <returns>The newly created customer.</returns>
        CustomerDto Add(CustomerCreateRequestDto customerDto);

        /// <summary>
        /// Update an existing customer.
        /// </summary>
        /// <param name="customerDto">The customer.</param>
        /// <returns>The updated customer.</returns>
        CustomerDto Update(CustomerDto customerDto);
        #endregion
    }
}