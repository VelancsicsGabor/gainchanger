﻿using AutoMapper;
using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using Repository.Interface;
using Repository.EntityModels;
using System;
using System.Collections.Generic;

namespace BusinessLogic.Manager
{
    public class CustomersManager : ICustomersManager
    {
        #region Fields

        /// <summary>
        /// The repository
        /// </summary>
        private readonly IRepository _repository;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// The locker
        /// </summary>
        private readonly TransferLocker _locker;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersManager"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="locker">The locker.</param>
        public CustomersManager(IRepository repository, IMapper mapper, TransferLocker locker)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _locker = locker ?? throw new ArgumentNullException(nameof(locker));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes the customerDto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCustomer(int id)
        {
            lock (_locker)
            {
                var customer = _repository.GetCustomer(id);
                if (customer != null)
                {
                    _repository.DeleteCustomer(customer);
                }
            }
        }

        /// <summary>
        /// Gets the customerDto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerDto GetCustomer(int id)
        {
            return _mapper.Map<CustomerDto>(_repository.GetCustomer(id));
        }

        /// <summary>
        /// Gets all the active customers
        /// </summary>
        /// <returns>List of active customers</returns>
        public IEnumerable<CustomerDto> GetCustomers()
        {
            return _mapper.Map<IEnumerable<CustomerDto>>(_repository.GetCustomers());
        }

        /// <summary>
        /// Check that the customer exists with the given id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns></returns>
        public bool DoesCustomerExists(int id)
        {
            return _repository.DoesCustomerExist(id);
        }

        /// <summary>
        /// Add new customer
        /// </summary>
        /// <param name="customerDto">The request.</param>
        /// <returns>The newly created customer.</returns>
        public CustomerDto Add(CustomerCreateRequestDto customerDto)
        {
            if (customerDto == null) throw new ArgumentNullException(nameof(customerDto));

            var entity = _repository.AddCustomer(_mapper.Map<CustomerEntity>(customerDto));
            return _mapper.Map<CustomerDto>(entity);
        }

        /// <summary>
        /// Update an existing customer.
        /// </summary>
        /// <param name="customerDto">The customer.</param>
        /// <returns>The updated customer.</returns>
        public CustomerDto Update(CustomerDto customerDto)
        {
            if (customerDto == null) throw new ArgumentNullException(nameof(customerDto));
            
            _repository.UpdateCustomer(_mapper.Map<CustomerEntity>(customerDto));
            return customerDto;
        }

        #endregion
    }
}