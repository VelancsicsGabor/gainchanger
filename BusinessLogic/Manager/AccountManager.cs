﻿using BusinessLogic.Manager.Interface;
using DataTransferObjects;
using Repository.Interface;
using System;

namespace BusinessLogic.Manager
{
    public class AccountManager : IAccountManager
    {
        #region Fields

        /// <summary>
        /// The repository
        /// </summary>
        private readonly IRepository _repository;

        /// <summary>
        /// The locker
        /// </summary>
        private readonly TransferLocker _locker;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountManager"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="locker">The locker.</param>
        public AccountManager(IRepository repository, TransferLocker locker)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _locker = locker ?? throw new ArgumentNullException(nameof(locker));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customerEntity identifier.</param>
        /// <returns>The funds.</returns>
        public decimal GetAvailableFunds(int customerId)
        {
            lock (_locker)
            {
                return _repository.GetAvailableFunds(customerId);
            }
        }

        /// <summary>
        /// Deposit the funds.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>The Result.</returns>
        public Result DepositFunds(int id, decimal funds)
        {
            lock (_locker)
            {
                var customer = _repository.GetCustomer(id);
                return customer == null ? Result.Failed($"Customer not found, id:{id}") : _repository.DepositFunds(customer, funds);
            }
        }

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result WithdrawFunds(int id, decimal funds)
        {
            lock (_locker)
            {
                var customer = _repository.GetCustomer(id);
                return customer == null
                    ? Result.Failed($"Customer not found, id:{id}")
                    : _repository.WithdrawFunds(customer, funds);
            }
        }

        /// <summary>
        /// Transfer the funds
        /// </summary>
        /// <param name="fromId">The from customer id.</param>
        /// <param name="toId">The to customer id.</param>
        /// <param name="funds">The funds.</param>
        /// <returns>Result</returns>
        public Result TransferFunds(int fromId, int toId, decimal funds)
        {
            lock (_locker)
            {
                var fromCustomer = _repository.GetCustomer(fromId);

                if (fromCustomer == null)
                {
                    return Result.Failed($"Customer not found, id:{fromId}");
                }

                var toCustomer = _repository.GetCustomer(toId);

                if (toCustomer == null)
                {
                    return Result.Failed($"Customer not found, id:{toId}");
                }

                return _repository.TransferFunds(fromCustomer, toCustomer, funds);
            }
        }

        #endregion
    }
}